from aiohttp import web
from uuid import uuid4, UUID
from peewee_async import Manager, PostgresqlDatabase, execute
import peewee
import psycopg2
from peewee import OperationalError
import json
from datetime import datetime
import asyncio
import logging
import sys
from time import sleep

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.INFO,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("API")

db = PostgresqlDatabase(database='chat', port='5432', user='chat', password='1234', host='postgres')

class BaseModel(peewee.Model):
    class Meta:
        database = db

class Users(BaseModel):
    id = peewee.UUIDField(primary_key=True)
    name = peewee.TextField(unique=True)
    created_at = peewee.DateTimeField(default=datetime.now)

class Chats(BaseModel):
    id = peewee.UUIDField(primary_key=True)
    name = peewee.TextField(unique=True)
    created_at = peewee.DateTimeField(default=datetime.now)

class User_Chat(BaseModel):
    id = peewee.UUIDField(default=uuid4(), primary_key=True)
    user_id = peewee.ForeignKeyField(Users)
    chat_id = peewee.ForeignKeyField(Chats)

class Message(BaseModel):
    id = peewee.UUIDField(default=uuid4(), primary_key=True)
    chat = peewee.ForeignKeyField(Chats)
    author = peewee.ForeignKeyField(Users)
    text = peewee.TextField()
    created_at = peewee.DateTimeField(default=datetime.now)

objects = Manager(db)

while True:
    try:
        Users.create_table()
        Chats.create_table()
        User_Chat.create_table()
        Message.create_table()
        break
    except OperationalError:
        logger.warning('Postgres starting...')
    except psycopg2.errors.DuplicateTable:
        logger.warning('Table is already exist!')
        break

class WebResp:
    def __init__(self, status, data={}):
        self.status = status
        self.data = data

    def __str__(self):
        return(json.dumps({'status': self.status, 'data': self.data}))

async def add_user(request):
    data = await request.json()

    try:
        username = data['username']
    except KeyError:
        return web.Response(status=400, body=(str(WebResp('Username is not defined'))))

    user_id = uuid4()

    try:
        await objects.create_or_get(Users, id=user_id, name=username)
    except:
        logger.info(f"user name '{username}' is already in db")
        return web.Response(status=400, body=(str(WebResp('User with this username is already exist!'))))

    return web.Response(status=200, body=str(WebResp('OK', {'user_id': str(user_id)})))

async def add_chat(request):
    data = await request.json()

    try:
        chat_name = data['name']
        users_ids = data['users']
    except KeyError:
        return web.Response(status=400, body=(str(WebResp('Chats name or users ids is not defined'))))

    users = await execute(Users.select())
    ids_from_bd = [user.id for user in users]

    for id in users_ids:
        try:
            if UUID(id) not in ids_from_bd:
                logger.info(f"User with '{id}' is not in {ids_from_bd}")
                return web.Response(status=404, body=(str(WebResp(f"Can't found user with '{id}' id!"))))
        except ValueError:
            return web.Response(status=400, body=(str(WebResp(f"Invalid id: '{id}'"))))

    chat_id = uuid4()

    if isinstance(users_ids, list) and len(set(users_ids)) > 1:
    
        if len(users_ids) != len(set(users_ids)):
            logger.info(f"Duplicate user ids for chat '{chat_name}'")
            users_ids = set(users_ids)

        try:
            await objects.create_or_get(Chats, id=chat_id, name=chat_name)
        except:
            logger.info(f"chat name '{chat_name}' is already in db")
            return web.Response(status=400, body=(str(WebResp('Chat is already exist!'))))

        for user_id in users_ids:
            try:
                await objects.create(User_Chat, id=uuid4(), user_id=user_id, chat_id=chat_id)
            except Exception as error:
                logger.error(f'{error}')
    else:
        return web.Response(status=400, body=(str(WebResp('Chat can be created at least between two users!'))))

    return web.Response(status=200, body=str(WebResp('OK', {'chat_id': str(chat_id)})))

async def add_message(request):
    data = await request.json()

    try:
        chat = data['chat']
        author = data['author']
        text = data['text']
    except KeyError:
        return web.Response(status=400, body=(str(WebResp('Some data is not defined!'))))

    try:
        chat = UUID(chat)
        author = UUID(author)
    except ValueError:
        return web.Response(status=400, body=(str(WebResp(f"Invalid id: '{chat}' or '{author}'!"))))

    message_id = uuid4()

    users = await execute(Users.select())
    chats = await execute(Chats.select())
    ids_from_users = [user.id for user in users]
    ids_from_chats = [chat.id for chat in chats]

    if author not in ids_from_users:
        return web.Response(status=400, body=(str(WebResp(f"User '{author}' does not exist!"))))
    if chat not in ids_from_chats:
        return web.Response(status=400, body=(str(WebResp(f"Chat '{chat}' does not exist!"))))

    user_chats = await execute(User_Chat.select(User_Chat.user_id).where(User_Chat.chat_id==chat))
    ids = [user.user_id.id for user in user_chats]
    
    if author not in ids:
        return web.Response(status=400, body=(str(WebResp(f"User '{author}' is not in chat '{chat}'!"))))

    await objects.create(Message, id=message_id, chat=chat, author=author, text=text)

    return web.Response(status=200, body=str(WebResp('OK', {'message_id': str(message_id)})))

async def get_chats(request):
    data = await request.json()

    try:
        user_id = data['user']
    except KeyError:
        return web.Response(status=400, body=(str(WebResp('User id is not defined!'))))

    try:
        user_id = UUID(user_id)
    except ValueError:
        return web.Response(status=400, body=(str(WebResp(f"Invalid id: '{user_id}'!"))))

    chats = await execute(Chats.select().join(User_Chat, on=(User_Chat.chat_id==Chats.id)).join(Message, on=(Chats.id==Message.chat_id)).where(User_Chat.user_id==str(user_id)).order_by(Message.created_at.desc()))

    parse_chats = {'chats': []}

    for chat in chats:
        chat_data = {'id':str(chat.id), 'chat_name':chat.name, 'created_at':str(chat.created_at)}
        if chat_data not in parse_chats['chats']:
            parse_chats['chats'].append(chat_data)

    return web.Response(status=200, body=str(WebResp('OK', parse_chats)))


async def get_messages(request):
    data = await request.json()

    try:
        chat_id = data['chat']
    except KeyError:
        return web.Response(status=400, body=(str(WebResp('Chat id is not defined!'))))

    try:
        chat_id = UUID(chat_id)
    except ValueError:
        return web.Response(status=400, body=(str(WebResp(f"Invalid id: '{chat_id}'!"))))

    messages = await execute(Message.select().join(Chats, on=(Message.chat_id==Chats.id)).where(Message.chat_id==str(chat_id)).order_by(Message.created_at))

    parse_messages = {'messages': []}
    for message in messages:
        message_data = {'id':str(message.id), 'chat':str(message.chat_id), 'author': str(message.author), 'text':message.text, 'created_at':str(message.created_at)}
        if message_data not in parse_messages['messages']:
            parse_messages['messages'].append(message_data)

    return web.Response(status=200, body=str(WebResp('OK', parse_messages)))

async def init_app():
    app = web.Application()

    app.add_routes([web.post('/users/add', add_user),
                    web.post('/chats/add', add_chat),
                    web.post('/messages/add', add_message),
                    web.post('/chats/get', get_chats),
                    web.post('/messages/get', get_messages)])
    return app

loop = asyncio.get_event_loop()
app = loop.run_until_complete(init_app())

web.run_app(app, port=9000)
